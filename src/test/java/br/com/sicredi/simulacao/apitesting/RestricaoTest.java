package br.com.sicredi.simulacao.apitesting;

import org.junit.Test;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import org.json.simple.JSONObject;

public class RestricaoTest extends MassaDeTestes{

	@Test
	public void testaCpfsComRestricao() {
		for(int indiceCpf = 0; indiceCpf < cpfsRestricao.length; indiceCpf++)
		{
			testaCpfComRestricao(cpfsRestricao[indiceCpf]);
		}
	}
		
	public void testaCpfComRestricao(String cpfEmTeste) {
		JSONObject request = new JSONObject();
		
		Response response = given().body(request.toJSONString()).when().get("http://localhost:8080/api/v1/restricoes/"+cpfEmTeste.toString());
		
		// "O CPF 99999999999 possui restrição"
		response.then().assertThat().statusCode(200).body("mensagem", containsString("O CPF " + cpfEmTeste + " possui restrição"));
		System.out.println("Cpf Testado: " + cpfEmTeste);
	}
	
	@Test
	public void testaCpfSemRestricao() {
		JSONObject request = new JSONObject();
		given().
		body(request.toJSONString()).
		when().
		get("http://localhost:8080/api/v1/restricoes/08273413500").
		//Se não possui restrição do HTTP Status 204 é retornado 
		then().statusCode(204).
		log().all();
	}	
}