package br.com.sicredi.simulacao.apitesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class SimulacaoTest extends MassaDeTestes {

	@BeforeClass
	public static void urlbase() {
		RestAssured.baseURI = "http://localhost:8080/api/v1/";
	}

	@Test
	public void criaSimulacao_validaRetorno201() {
		Response response = given()
				.contentType("application/json")
				.body(corpoSimulacao)
				.when()
				.post("simulacoes");
		// Simulação cadastrada com sucesso retorna o HTTP Status 201
		response.then().statusCode(201);
		// Testar se os dados inseridos condizem com os dados retornados
		System.out.println("RETORNO DA API CRIAR SIMUL.=> " + response.body().asString());
		// Uma simulação com problema em alguma regra retorna o HTTP Status 400 com a
		// lista de erros
	}

	@Test
	public void criaSimulacao_validaRetorno409Duplicada() {
		// Uma simulação para um mesmo CPF retorna um HTTP Status 409 com a mensagem
		// "CPF já existente"
		Response response = given().contentType("application/json").body(corpoSimulacaoDuplicada).when()
				.post("simulacoes");
		response = given().contentType("application/json").body(corpoSimulacaoDuplicada).when().post("simulacoes");
		// Status code 409 para simulação criada para cpf que já tem simulação
		response.then().assertThat().
		statusCode(409).
		body("mensagem", containsString("CPF já existente")); // BUG, retornando "CPF duplicado"
		System.out.println(
				"criaSimulacaoDuplicada => RETORNO DA API CRIAR SIMUL. DUPLICADA => " + response.body().asString());
	}

	@Test
	public void alteraSimulacaoCpfExistente_validaRetorno200() {
		//pega todas simulacoes existentes, pega o primeiro cpf
		Response responseSimulacoes = given().contentType("application/json").when().get("simulacoes");

		// Pega todos os IDs para recuperar o primeiro, faz replace e split
		String cpfsRetornados = responseSimulacoes.jsonPath().getString("cpf");
		cpfsRetornados = cpfsRetornados.replace("[", "");
		cpfsRetornados = cpfsRetornados.replace("]", "");
		String[] cpfsTratados = cpfsRetornados.split(",");
		
		String cpfUpdate = cpfsTratados[0];
		String corpoSimulacaoUpdated = "{\"nome\":\"Teste Update Simulacao CPF\",\"cpf\":\"" + cpfUpdate  + "\",\"email\":\"updated@gmail.com\",\"valor\":20000.00,\"parcelas\":36,\"seguro\":false}";
		
		System.out.println("CorpoUpdated: " + corpoSimulacaoUpdated);
		
		//Get da simulação do CPF filtrado
		Response response = given()
			.contentType("application/json")
			.pathParam("cpf", cpfUpdate)	
			.get("simulacoes/{cpf}");
		response.then().statusCode(200);
		
		//Faz Update da Simulacao pelo CPF
		Response responseUpdate = given()
				.contentType("application/json")
				.pathParam("cpf", cpfUpdate)
				.body(corpoSimulacaoUpdated)
				.when()
				.put("simulacoes/{cpf}");
		responseUpdate.then().statusCode(200);
		
		//Get da simulação do CPF filtrado novamente para checar resultado
		response = given()
			.contentType("application/json")
			.pathParam("cpf", cpfUpdate)	
			.get("simulacoes/{cpf}");
		
		// Simulação cadastrada com sucesso retorna o HTTP Status 201
		response
		.then()
		.assertThat()
		.statusCode(200)
		.and()
		.body("nome", containsString("Teste Update Simulacao CPF"));
		
		System.out.println("alteraSimulacaoCpfExistente => RETORNO DA API => " + response.body().asString());
		System.out.println("-----------------------------------------------");
	}

	public void alteraSimulacaoCpfInexistente_validaRetorno404() {
		String cpfUpdate = "00000000000";
		//Faz Update da Simulacao pelo CPF
		Response responseUpdate = given()
				.contentType("application/json")
				.pathParam("cpf", cpfUpdate)
				.body(corpoSimulacaoUpdated)
				.when()
				.put("simulacoes/{cpf}");
		responseUpdate.then().statusCode(404);
	}

	@Test
	public void consultaSimulacaoCpfExistente() {
		Response response = given().pathParam("cpf", cpfNovaSimulacao).when().get("simulacoes/{cpf}");
		response.then().statusCode(200);
		System.out.println("consultaSimulacaoCpfExistente => RETORNO DA API => " + response.body().asString());
		System.out.println("-----------------------------------------------");
	}

	@Test
	public void consultaSimulacaoCpfInexistente() {
		Response response = given().pathParam("cpf", "00000000000").when().get("simulacoes/{cpf}");
		response.then().statusCode(404);
		System.out.println("consultaSimulacaoCpfInexistente => RETORNO DA API => " + response.body().asString());
		System.out.println("-----------------------------------------------");
	}
	
	@Test
	public void consultaTodasSimulacoes_validaRetorno200() {
		Response response = given().contentType("application/json").when().get("simulacoes");
		System.out.println("RETORNO DA API => " + response.body().asString());
		response.then().statusCode(200);
		// OBS: Considerando que a API sobe no MAVEN sempre com dados, achei
		// desnecessário fazer o teste se o retorno estiver vindo vazio.
		// Se vier vazio, vai estourar erro.;
	}

	// Metodo usado pelo teste "consultaTodasSimulacoes_validaRetorno204"
	public void removeSimulacaoPeloID(String idRecebido) {
		Response response = given().pathParam("id", idRecebido).when().delete("simulacoes/{id}");

		response.then().statusCode(204);
		System.out.println("RETORNO DA API => " + response.body().asString());
		System.out.println("-----------------------------------------------");
	}

	@Test
	public void consultaTodasSimulacoes_validaRetorno204QuandoBaseVazia4() {
		Response response = given().contentType("application/json").when().get("simulacoes");

		// Pega todos os IDs para deletar, faz replace e split
		idsRetornados = response.jsonPath().getString("id");
		// System.out.println("idsRetornados:" + idsRetornados);
		idsRetornados = idsRetornados.replace("[", "");
		idsRetornados = idsRetornados.replace("]", "");
		String[] idsDelete = idsRetornados.split(",");

		// Remove todos os registros
		for (int indiceId = 0; indiceId < idsDelete.length; indiceId++) {
			System.out.println("id Delete Loop:" + idsDelete[indiceId]);
			removeSimulacaoPeloID(idsDelete[indiceId]);
		}

		response = given().contentType("application/json").when().get("simulacoes");
		response.then().statusCode(204); // BUG, retornando 200.;
	}

	@Test
	public void removeSimulacaoPeloID_validaRetorno204() {
		int idDelete = 11;
		Response response = given().pathParam("id", idDelete).when().delete("simulacoes/{id}");
		response.then().statusCode(200);
		System.out.println("removeSimulacaoPeloID_validaRetorno204 => RETORNO DA API => " + response.body().asString());
		System.out.println("-----------------------------------------------");
	}

	@Test
	public void removeSimulacaoPeloID_validaRetorno404() {
		int idDelete = 999999;
		Response response = given().pathParam("id", idDelete).when().delete("simulacoes/{id}");
		response.then().statusCode(404);
		System.out.println("removeSimulacaoPeloID_validaRetorno404 => RETORNO DA API => " + response.body().asString());
		System.out.println("-----------------------------------------------");
	}

}
