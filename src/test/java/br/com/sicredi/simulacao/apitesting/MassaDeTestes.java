package br.com.sicredi.simulacao.apitesting;

public class MassaDeTestes {
	int idSimulacao;
	String idSimulacaoCriadaStr;
	String idsRetornados;
	String[] cpfsRestricao = new String[] {"97093236014","60094146012","84809766080","62648716050","26276298085","01317496094","55856777050","19626829001","24094592008","58063164083"};
	String cpfNovaSimulacao = "11111111988";
	String cpfNovaSimulacaoDuplicada = "11122244455";
	
	String corpoSimulacao = "{\"nome\":\"Teste API\",\"cpf\":\"" + cpfNovaSimulacao  + "\",\"email\":\"teste@gmail.com\",\"valor\":20000.00,\"parcelas\":36,\"seguro\":false}";
	String corpoSimulacaoUpdated = "{\"nome\":\"Teste API\",\"cpf\":\"" + cpfNovaSimulacao  + "\",\"email\":\"updated@gmail.com\",\"valor\":20000.00,\"parcelas\":36,\"seguro\":false}";
	String corpoSimulacaoDuplicada = "{\"nome\":\"Teste API\",\"cpf\":\"" + cpfNovaSimulacaoDuplicada  + "\",\"email\":\"teste@gmail.com\",\"valor\":20000.00,\"parcelas\":36,\"seguro\":false}";
}
